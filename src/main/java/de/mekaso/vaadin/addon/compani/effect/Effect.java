package de.mekaso.vaadin.addon.compani.effect;

/**
 * Interface for all effect types.
 */
public interface Effect {
	/**
	 * Get the effect vendor library.
	 * @return the effect vendor
	 */
	public EffectVendor getEffectVendor();
	/**
	 * Get the name of the effect.
	 * 
	 * @return the name of the effect.
	 */
	public String name();
	
	/**
	 * Get the animation type of an effect.
	 * @return the animation type
	 */
	public AnimationType getAnimationType();
	
	/**
	 * Get the animation stage of an effect.
	 * @return the animation stage
	 */
	public AnimationStage getAnimationStage();
}
