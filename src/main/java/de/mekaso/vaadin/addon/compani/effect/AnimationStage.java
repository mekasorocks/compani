package de.mekaso.vaadin.addon.compani.effect;

/**
 * The stage (moment of time) of an animation.
 */
public enum AnimationStage {
	ENTRANCE, DISPLAY, EXIT
}
