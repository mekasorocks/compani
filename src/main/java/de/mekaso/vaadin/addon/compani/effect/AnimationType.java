package de.mekaso.vaadin.addon.compani.effect;

/**
 * All animation types.
 */
public enum AnimationType {
	COMPONENT, TEXT, VIEW
}
