package de.mekaso.vaadin.addon.compani.viewtransitions;

import java.util.stream.Stream;

public enum ViewInTransition {
	MoveFromRight("moveFromRight"),
	MoveFromLeft("moveFromLeft"),
	MoveFromBottom("moveFromBottom"),
	MoveFromTop("moveFromTop"),
	MoveFromRightOnTop("moveFromRight ontop"),
	MoveFromLeftOnTop("moveFromLeft ontop"),
	MoveFromBottomOnTop("moveFromBottom ontop"),
	MoveFromTopOnTop("moveFromTop ontop"),
	FadeFromRight("moveFromRightFade"),
	FadeFromLeft("moveFromLeftFade"),
	FadeFromBottom("moveFromBottomFade"),
	FadeFromTop("moveFromTopFade"),
	ScaleUpDown("scaleUpDown delay300"),
	ScaleUp("scaleUp delay300"),
	ScaleUpCenter("scaleUpCenter delay400"),
	FlipLeft("flipInLeft delay500"),
	FlipRight("flipInRight delay500"),
	FlipBottom("flipInBottom delay500"),
	FlipTop("flipInTop delay500"),
	News("rotateInNewspaper delay500"),
	RotatePullRight("rotatePullRight delay180"),
	RotatePullLeft("rotatePullLeft delay180"),
	RotatePullBottom("rotatePullBottom delay180"),
	RotatePullTop("rotatePullTop delay180"),
	MoveFromRightFade("moveFromRightFade"),
	MoveFromLeftFade("moveFromLeftFade"),
	MoveFromBottomFade("moveFromBottomFade"),
	MoveFromTopFade("moveFromTopFade"),
	RotateUnfoldLeft("rotateUnfoldLeft"),
	RotateUnfoldRight("rotateUnfoldRight"),
	RotateUnfoldTop("rotateUnfoldTop"),
	RotateUnfoldBottom("rotateUnfoldBottom"),
	RotateRoomLeftIn("rotateRoomLeftIn"),
	RotateRoomRightIn("rotateRoomRightIn"),
	RotateRoomTopIn("rotateRoomTopIn"),
	RotateRoomBottomIn("rotateRoomBottomIn"),
	RotateCubeLeftIn("rotateCubeLeftIn"),
	RotateCubeRightIn("rotateCubeRightIn"),
	RotateCubeTopIn("rotateCubeTopIn"),
	RotateCubeBottomIn("rotateCubeBottomIn"),
	RotateCarouselLeftIn("rotateCarouselLeftIn"),
	RotateCarouselRightIn("rotateCarouselRightIn"),
	RotateCarouselTopIn("rotateCarouselTopIn"),
	RotateCarouselBottomIn("rotateCarouselBottomIn"),
	RotateSidesIn("rotateSidesIn delay200"),
	RotateSlideIn("rotateSlideIn"),
	
	;
	private static final String PREFIX = "pt-page-";
	
	private String inClass;
	private ViewInTransition(String inClass) {
		this.inClass = inClass;
	}
	
	/**
	 * @return the inClass
	 */
	public String[] getInClasses() {
		return Stream.of(this.inClass.split(" ")).map(part -> PREFIX + part).toArray(String[]::new);
	}
}
