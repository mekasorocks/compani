package de.mekaso.vaadin.addon.compani.effect;

public enum ShowEffect implements Effect {
	fadeIn(EffectVendor.AnimateCss), 
	flipInX(EffectVendor.AnimateCss), 
	flipInY(EffectVendor.AnimateCss), 
	jackInTheBox(EffectVendor.AnimateCss), 
	powerOn(EffectVendor.Loading),
	rotateIn(EffectVendor.AnimateCss),
	rotateInDownLeft(EffectVendor.AnimateCss), 
	rotateInDownRight(EffectVendor.AnimateCss), 
	rotateInUpLeft(EffectVendor.AnimateCss), 
	rotateInUpRight(EffectVendor.AnimateCss), 
	vortexIn(EffectVendor.Loading),
	zoomIn(EffectVendor.AnimateCss), 
	zoomInDown(EffectVendor.AnimateCss), 
	zoomInLeft(EffectVendor.AnimateCss), 
	zoomInRight(EffectVendor.AnimateCss), 
	zoomInUp(EffectVendor.AnimateCss),
	;

	private EffectVendor effectVendor;
	private ShowEffect(EffectVendor effectVendor) {
		this.effectVendor = effectVendor;
	}
	
	@Override
	public EffectVendor getEffectVendor() {
		return this.effectVendor;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.COMPONENT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.DISPLAY;
	}
}