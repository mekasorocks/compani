package de.mekaso.vaadin.addon.compani.effect;

/**
 * The animation speed.
 */
public enum Speed {
	slower, 
	slow, 
	normal, 
	fast, 
	faster
}