package de.mekaso.vaadin.addon.compani.effect;

public enum TextExitEffect implements TextEffect {
	FadeOut("leFadeOut"),
	FadeOutLeft("leFadeOutLeft"),
	FadeOutRight("leFadeOutRight"),
	FadeOutTop("leFadeOutTop"),
	FadeOutBottom("leFadeOutBottom"),
	ScaleXOut("leScaleXOut"),
	ScaleYOut("leScaleYOut"),
	FlyOutTop("leFlyOutTop"),
	FlyOutRight("leFlyOutRight"),
	FlyOutLeft("leFlyOutLeft"),
	FlyOutBottom("leFlyOutBottom"),
	DoorOpenRight("leDoorOpenRight"),
	DoorOpenLeft("leDoorOpenLeft"),
	HangDropLeft("leHangAndDropLeft"),
	HangDropRight("leHangAndDropRight"),
	PushReleaseTo("lePushReleaseTo"),
	PushReleaseToTop("lePushReleaseToTop"),
	PushReleaseToBottom("lePushReleaseToBottom"),
	FlipOutTop("leFlipOutTop"),
	RollToLeft("leRollToLeft"),
	RollToRight("leRollToRight"),
	RollToTop("leRollToTop"),
	RollToBottom("leRollToBottom"),
	RotateOutSkateRight("leRotateSkateOutRight"),
	RotateOutSkateLeft("leRotateSkateOutLeft"),
	RotateOutSkateTop("leRotateSkateOutTop"),
	RotateOutSkateBottom("leRotateSkateOutBottom"),
	RotateXZoomOut("leRotateXZoomOut"),
	RotateYZoomOut("leRotateYZoomOut"),
	RotateOut("leRotateOut"),
	RotateOutLeft("leRotateOutLeft"),
	RotateOutRight("leRotateOutRight"),
	SpinOutLeft("leSpinOutLeft"),
	SpinOutRight("leSpinOutRight"),
	BlurOut("leBlurOut"),
	BlurOutRight("leBlurOutRight"),
	BlurOutLeft("leBlurOutLeft"),
	BlurOutTop("leBlurOutTop"),
	BlurOutBottom("leBlurOutBottom"),
	PerspectiveOutTop("lePerspectiveOutTop"),
	PerspectiveOutBottom("lePerspectiveOutBottom"),
	ZoomOut("leZoomOut"),
	ZoomOutLeft("leZoomOutLeft"),
	ZoomOutRight("leZoomOutRight"),
	ZoomOutTop("leZoomOutTop"),
	ZoomOutBottom("leZoomOutBottom"),
	DanceOutTop("leDanceOutTop"),
	DanceOutMiddle("leDanceOutMiddle"),
	DanceOutBottom("leDanceOutBottom"),
	OneAfterOneFadeOut("oaoFadeOut"),
	OneAfterOneFlyOut("oaoFlyOut"),
	OneAfterOneRotateOut("oaoRotateOut"),
	OneAfterOneRotateXOut("oaoRotateXOut"),
	OneAfterOneRotateYOut("oaoRotateYOut")
	;
	
	private String cssClass;

	private TextExitEffect(String cssClass) {
		this.cssClass = cssClass;
	}

	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.TEXT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.EXIT;
	}

	@Override
	public EffectVendor getEffectVendor() {
		return EffectVendor.CssAnimation;
	}
}