package de.mekaso.vaadin.addon.compani.viewtransitions;

import java.util.stream.Stream;

public enum ViewOutTransition {
	MoveToLeft("moveToLeft"),
	MoveToRight("moveToRight"),
	MoveToTop("moveToTop"),
	MoveToBottom("moveToBottom"),
	Fade("fade"),
	FadeToLeft("moveToLeftFade"),
	FadeToRight("moveToRightFade"),
	FadeToTop("moveToTopFade"),
	FadeToBottom("moveToBottomFade"),
	EaseToLeft("moveToLeftEasing ontop"),
	EaseToRight("moveToRightEasing ontop"),
	EaseToTop("moveToTopEasing ontop"),
	EaseToBottom("moveToBottomEasing ontop"),
	ScaleDown("scaleDown"),
	ScaleDownUp("scaleDownUp"),
	ScaleDownCenter("scaleDownCenter"),
	RotateRight("rotateRightSideFirst"),
	RotateLeft("rotateLeftSideFirst"),
	RotateTop("rotateTopSideFirst"),
	RotateBottom("rotateBottomSideFirst"),
	FlipRight("flipOutRight"),
	FlipLeft("flipOutLeft"),
	FlipTop("flipOutTop"),
	FlipBottom("flipOutBottom"),
	RotateFall("rotateFall ontop"),
	News("rotateOutNewspaper"),
	RotatePushLeft("rotatePushLeft"),
	RotatePushRight("rotatePushRight"),
	RotatePushTop("rotatePushTop"),
	RotatePushBottom("rotatePushBottom"),
	RotateFoldLeft("rotateFoldLeft"),
	RotateFoldRight("rotateFoldRight"),
	RotateFoldTop("rotateFoldTop"),
	RotateFoldBottom("rotateFoldBottom"),
	MoveToRightFade("moveToRightFade"),
	MoveToLeftFade("moveToLeftFade"),
	MoveToBottomFade("moveToBottomFade"),
	MoveToTopFade("moveToTopFade"),
	RotateRoomLeftOut("rotateRoomLeftOut ontop"),
	RotateRoomRightOut("rotateRoomRightOut ontop"),
	RotateRoomTopOut("rotateRoomTopOut ontop"),
	RotateRoomBottomOut("rotateRoomBottomOut ontop"),
	RotateCubeLeftOut("rotateCubeLeftOut ontop"),
	RotateCubeRightOut("rotateCubeRightOut ontop"),
	RotateCubeTopOut("rotateCubeTopOut ontop"),
	RotateCubeBottomOut("rotateCubeBottomOut ontop"),
	RotateCarouselLeftOut("rotateCarouselLeftOut ontop"),
	RotateCarouselRightOut("rotateCarouselRightOut ontop"),
	RotateCarouselTopOut("rotateCarouselTopOut ontop"),
	RotateCarouselBottomOut("rotateCarouselBottomOut ontop"),
	RotateSidesOut("rotateSidesOut"),
	RotateSlideOut("rotateSlideOut")
	
	
	;
	private static final String PREFIX = "pt-page-";
	private String outClass;
	private ViewOutTransition(String outClass) {
		this.outClass = outClass;
	}
	
	/**
	 * @return the outClass
	 */
	public String[] getOutClasses() {
		return Stream.of(this.outClass.split(" ")).map(part -> PREFIX + part).toArray(String[]::new);
	}
}
