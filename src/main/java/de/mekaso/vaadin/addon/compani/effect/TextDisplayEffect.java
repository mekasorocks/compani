package de.mekaso.vaadin.addon.compani.effect;

/**
 * Effects for text components 
 * @see 	com.vaadin.flow.component.html.Anchor
 * @see		com.vaadin.flow.component.html.Div
 * @see 	com.vaadin.flow.component.html.H1
 * @see 	com.vaadin.flow.component.html.H2
 * @see 	com.vaadin.flow.component.html.H3
 * @see 	com.vaadin.flow.component.html.H4
 * @see 	com.vaadin.flow.component.html.H5
 * @see 	com.vaadin.flow.component.html.H6
 * @see 	com.vaadin.flow.component.html.Label
 * @see 	com.vaadin.flow.component.html.ListItem
 * @see 	com.vaadin.flow.component.html.Paragraph
 * @see 	com.vaadin.flow.component.html.Span
 * .
 */
public enum TextDisplayEffect implements TextEffect {
	HuHu("hu__hu__"),
	Snake("leSnake"),
	Peek("lePeek"),
	Effect3D("effect3d"),
	RainDrop("leRainDrop"),
	PePe("pepe"),
	WaterWave("leWaterWave"),
	Lightning("lightning"),
	JoltZoom("leJoltZoom"),
	Electricity("electricity"),
	Magnify("leMagnify"),
	LetterBeat("leBeat"),
	
	BackFromRight("leMovingBackFromRight"),
	BackFromLeft("leMovingBackFromLeft"),
	
	KickOutFront("leKickOutFront"),
	KickOutBehind("leKickOutBehind"),
	
	SkateLeftRight("leSkateX"),
	SkateBottomTop("leSkateY"),
	SkateBoth("leSkateXY"),
	
	LetterJump("leJump"),
	
	AboundTop("leAboundTop"),
	AboundBottom("leAboundBottom"),
	AboundLeft("leAboundLeft"),
	AboundRight("leAboundRight"),
	
	Recontre("leRencontre"),
	PulseShake("lePulseShake"),
	HorizonalShake("leHorizontalShake"),
	VerticalShake("leVerticalShake"),
	MadMax("leMadMax"),
	HorizontalTremble("leHorizontalTremble"),
	VerticalTremble("leVerticalTremble"),
	CrazyCool("leCrazyCool"),
	Vibration("leVibration"),

	PopUp("lePopUp"),
	PopUpLeft("lePopUpLeft"),
	PopUpRight("lePopUpRight"),
	
	PopOut("lePopOut"),
	PopOutLeft("lePopOutLeft"),
	PopOutRight("lePopOutRight"),
	
	BounceFromTop("leBounceFromTop"),
	BounceFromDown("leBounceFromDown"),
	BounceY("leBounceY"),
	BounceZoomIn("leBounceZoomIn"),
	BounceZoomOut("leBounceZoomOut"),
	
	;
	
	private String cssClass;
	private TextDisplayEffect(String cssClass) {
		this.cssClass = cssClass;
	}
	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}
	@Override
	public AnimationType getAnimationType() {
		return AnimationType.TEXT;
	}
	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.DISPLAY;
	}
	@Override
	public EffectVendor getEffectVendor() {
		return EffectVendor.CssAnimation;
	}
}