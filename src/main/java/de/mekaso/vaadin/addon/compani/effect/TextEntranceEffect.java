package de.mekaso.vaadin.addon.compani.effect;

public enum TextEntranceEffect implements TextEffect {
	Typing("typing"), FadeIn("leFadeIn"), FadeInLeft("leFadeInLeft"), FadeInRight("leFadeInRight"),
	FadeInTop("leFadeInTop"), FadeInBottom("leFadeInBottom"), ScaleXIn("leScaleXIn"), ScaleYIn("leScaleYIn"),
	FlyInTop("leFlyInTop"), FlyInRight("leFlyInRight"), FlyInLeft("leFlyInLeft"), FlyInBottom("leFlyInBottom"),
	DoorCloseLeft("leDoorCloseLeft"), DoorCloseRight("leDoorCloseRight"), PushReleaseFrom("lePushReleaseFrom"),
	PushReleaseFromLeft("lePushReleaseFromLeft"), PushReleaseFromTop("lePushReleaseFromTop"),
	PushReleaseFromBottom("lePushReleaseFromBottom"), FlipInTop("leFlipInTop"), FlipInBottom("leFlipInBottom"),
	FlipOutBottom("leFlipOutBottom"), ElevateLeft("leElevateLeft"), ElevateRight("leElevateRight"),
	RollFromLeft("leRollFromLeft"), RollFromRight("leRollFromRight"), RollFromTop("leRollFromTop"),
	RollFromBottom("leRollFromBottom"), RotateInSkateRight("leRotateSkateInRight"),
	RotateInSkateLeft("leRotateSkateInLeft"), RotateInSkateTop("leRotateSkateInTop"),
	RotateInSkateBottom("leRotateSkateInBottom"), RotateXZoomIn("leRotateXZoomIn"), RotateYZoomIn("leRotateYZoomIn"),
	RotateIn("leRotateIn"), RotateInLeft("leRotateInLeft"), RotateInRight("leRotateInRight"),
	SpinInLeft("leSpinInLeft"), SpinInRight("leSpinInRight"), BlurIn("leBlurIn"), BlurInRight("leBlurInRight"),
	BlurInLeft("leBlurInLeft"), BlurInTop("leBlurInTop"), BlurInBottom("leBlurInBottom"), ZoomIn("leZoomIn"),
	ZoomInLeft("leZoomInLeft"), ZoomInRight("leZoomInRight"), ZoomInTop("leZoomInTop"), ZoomInBottom("leZoomInBottom"),
	DanceInTop("leDanceInTop"), DanceInMiddle("leDanceInMiddle"), DanceInBottom("leDanceInBottom"),
	OneAfterOneFadeIn("oaoFadeIn"), OneAfterOneFlyIn("oaoFlyIn"), OneAfterOneRotateIn("oaoRotateIn"),
	OneAfterOneRotateXIn("oaoRotateXIn"), OneAfterOneRotateYIn("oaoRotateYIn"),;

	private String cssClass;

	private TextEntranceEffect(String cssClass) {
		this.cssClass = cssClass;
	}

	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.TEXT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.ENTRANCE;
	}

	@Override
	public EffectVendor getEffectVendor() {
		return EffectVendor.CssAnimation;
	}
}