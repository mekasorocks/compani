package de.mekaso.vaadin.addon.compani.viewtransitions;

import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ViewTransition {
	MoveFromRightAndMoveToLeft("moveToLeft", "moveFromRight"),
	MoveToRightAndMoveFromLeft("moveToRight", "moveFromLeft"),
	MoveToTopAndMoveFromBottom("moveToTop", "moveFromBottom"),
	MoveToBottomAndMoveFromTop("moveToBottom", "moveFromTop"),
	FadeAndMoveFromRight("fade", "moveFromRight ontop"),
	FadeAndMoveFromLeft("fade", "moveFromLeft ontop"),
	FadeAndMoveFromBottom("fade", "moveFromBottom ontop"),
	FadeAndMoveFromTop("fade", "moveFromTop ontop"),
	FadeToLeftAndFadeFromRight("moveToLeftFade", "moveFromRightFade"),
	FadeToRightAndFadeFromLeft("moveToRightFade", "moveFromLeftFade"),
	FadeToTopAndFadeFromBottom("moveToTopFade", "moveFromBottomFade"),
	FadeToBottomAndFadeFromTop("moveToBottomFade", "moveFromTopFade"),
	EaseToLeftAndMoveFromRight("moveToLeftEasing ontop", "moveFromRight"),
	EaseToRightAndMoveFromLeft("moveToRightEasing ontop", "moveFromLeft"),
	EaseToTopAndMoveFromBottom("moveToTopEasing ontop", "moveFromBottom"),
	EaseToBottomAndMoveFromTop("moveToBottomEasing ontop", "moveFromTop"),
	ScaleDownAndMoveFromRight("scaleDown", "moveFromRight ontop"),
	ScaleDownAndMoveFromLeft("scaleDown", "moveFromLeft ontop"),
	ScaleDownAndMoveFromBottom("scaleDown", "moveFromBottom ontop"),
	ScaleDownAndMoveFromTop("scaleDown", "moveFromTop ontop"),
	ScaleDownAndScaleUpDown("scaleDown", "scaleUpDown delay300"),
	ScaleDownAndUpScaleUp("scaleDownUp", "scaleUp delay300"),
	MoveToLeftAndScaleUp("moveToLeft ontop", "scaleUp"),
	MoveToRightAndScaleUp("moveToRight ontop", "scaleUp"),
	MoveToTopAndScaleUp("moveToTop ontop", "scaleUp"),
	MoveToBottomAndScaleUp("moveToBottom ontop", "scaleUp"),
	ScaleDownCenterAndScaleUpCenter("scaleDownCenter", "scaleUpCenter delay400"),
	RotateRightAndMoveFromRight("rotateRightSideFirst", "moveFromRight delay200 ontop"),
	RotateLeftAndMoveFromLeft("rotateLeftSideFirst", "moveFromLeft delay200 ontop"),
	RotateTopAndMoveFromTop("rotateTopSideFirst", "moveFromTop delay200 ontop"),
	RotateBottomAndMoveFromBottom("rotateBottomSideFirst", "moveFromBottom delay200 ontop"),
	FlipRightAndFlipLeft("flipOutRight", "flipInLeft delay500"),
	FlipLeftAndFlipRight("flipOutLeft", "flipInRight delay500"),
	FlipTopAndFlipBottom("flipOutTop", "flipInBottom delay500"),
	FlipBottomAndFlipTop("flipOutBottom", "flipInTop delay500"),
	RotateFallAndScaleUp("rotateFall ontop", "scaleUp"),
	NewsAndNews("rotateOutNewspaper", "rotateInNewspaper delay500"),
	RotateLeftAndMoveFromRight("rotatePushLeft", "moveFromRight"),
	RotateRightAndMoveFromLeft("rotatePushRight", "moveFromLeft"),
	RotateTopAndMoveFromBottom("rotatePushTop", "moveFromBottom"),
	RotateBottomAndMoveFromTop("rotatePushBottom", "moveFromTop"),
	//42
	
	
	
	;
	private static final String OUT_TEXT = "current view will '%s' and the target view will '%s'";
	private static final String PREFIX = "pt-page-";
	private static final Pattern CAMEL_CASE = Pattern.compile("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
	private String outClass;
	private String inClass;
	
	private ViewTransition(String outClass, String inClass) {
		this.outClass = outClass;
		this.inClass = inClass;
	}
	/**
	 * @return the inClass
	 */
	public String[] getInClasses() {
		return Stream.of(this.inClass.split(" ")).map(part -> PREFIX + part).toArray(String[]::new);
	}
	/**
	 * @return the outClass
	 */
	public String[] getOutClasses() {
		return Stream.of(this.outClass.split(" ")).map(part -> PREFIX + part).toArray(String[]::new);
	}
	
	@Override
	public String toString() {
		return String.format(OUT_TEXT, CAMEL_CASE.splitAsStream(outClass).collect(Collectors.joining(" ")), CAMEL_CASE.splitAsStream(inClass).collect(Collectors.joining(" ")));
	}
}
