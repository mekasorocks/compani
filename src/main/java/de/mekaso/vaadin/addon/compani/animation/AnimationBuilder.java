package de.mekaso.vaadin.addon.compani.animation;

import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.HasStyle;

/**
 * @author Meik Kaufmann
 * Builder for animation of all types.
 */
public class AnimationBuilder {
	private static final Logger logger = LoggerFactory.getLogger(AnimationBuilder.class);
	
	private static AnimationBuilder animationBuilder;
	private HasStyle animatedComponent;
	
	/**
	 * Create an animation builder.
	 * @param animatedComponent the animated component
	 * @return the builder
	 */
	public static AnimationBuilder createBuilderFor(HasStyle animatedComponent) {
		animationBuilder = new AnimationBuilder(animatedComponent);
		return animationBuilder;
	}
	
	private AnimationBuilder(HasStyle animatedComponent) {
		this.animatedComponent = animatedComponent;
	}
	
	/**
	 * Create an animation.
	 * @param <T> the type
	 * @param type the type class
	 * @return an animation
	 */
	public <T extends Animation> T create(Class<T> type) {
		Animation animation = null;
		try {
			animation = type.getDeclaredConstructor().newInstance();
			animation.animatedComponent = this.animatedComponent;
			if (!animation.isTextEffect()) {
				animation.domListenerRegistration = this.animatedComponent.getElement().addEventListener(AnimationEndEvent.NAME, animation);
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			logger.warn("Could not create animation", e);
		}
		return type.cast(animation);
	}
}