package de.mekaso.vaadin.addon.compani.effect;

public enum HideEffect implements Effect {
	fadeOut(EffectVendor.AnimateCss), 
	flipOutX(EffectVendor.AnimateCss), 
	flipOutY(EffectVendor.AnimateCss),
	powerOff(EffectVendor.Loading),
	rotateOut(EffectVendor.AnimateCss), 
	rotateOutDownLeft(EffectVendor.AnimateCss), 	
	rotateOutDownRight(EffectVendor.AnimateCss), 
	rotateOutUpLeft(EffectVendor.AnimateCss), 
	rotateOutUpRight(EffectVendor.AnimateCss),
	vortexOut(EffectVendor.Loading),
	zoomOut(EffectVendor.AnimateCss), 
	zoomOutDown(EffectVendor.AnimateCss), 
	zoomOutLeft(EffectVendor.AnimateCss), 
	zoomOutRight(EffectVendor.AnimateCss), 
	zoomOutUp(EffectVendor.AnimateCss), 
	;

	private EffectVendor effectVendor;
	private HideEffect(EffectVendor effectVendor) {
		this.effectVendor = effectVendor;
	}
	
	@Override
	public EffectVendor getEffectVendor() {
		return this.effectVendor;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.COMPONENT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.DISPLAY;
	}
}