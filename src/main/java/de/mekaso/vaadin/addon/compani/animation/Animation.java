package de.mekaso.vaadin.addon.compani.animation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.shared.Registration;

import de.mekaso.vaadin.addon.compani.effect.Effect;
import de.mekaso.vaadin.addon.compani.effect.ExitEffect;
import de.mekaso.vaadin.addon.compani.effect.HideEffect;
import de.mekaso.vaadin.addon.compani.effect.TextEffect;
import de.mekaso.vaadin.addon.compani.effect.TextExitEffect;

/**
 * The animation for a component.
 */
@Tag("compani-animation")
public abstract class Animation extends Component implements DomEventListener {
	private static final long serialVersionUID = 1L;
	/**
	 * Url of the stylesheet for the addon. Must be included in one of your layouts.
	 */
	public static final String STYLES = "https://cdn.jsdelivr.net/gh/mekaso-de/compani/styles.css";
	/**
	 * an effect.
	 */
	protected Effect effect;
	/**
	 * the animated component;
	 */
	protected HasStyle animatedComponent;
	
	private Registration animationEndListenerRegistration;
	
	protected DomListenerRegistration domListenerRegistration;
	
	protected String [] currentEffect;

	protected abstract String[] fetchCssClasses();
	
	/**
	 * Start an animation.
	 */
	public void start() {
		this.currentEffect = fetchCssClasses();
		this.animatedComponent.addClassNames(this.currentEffect);
	}
	
	/**
	 * Stop an animation.
	 */
	public void stop() {
		if (this.currentEffect != null) {
			this.animatedComponent.removeClassNames(this.currentEffect);
			this.currentEffect = null;
		}
	}
	

	/**
	 * Register an listener that is triggered by an animation end event.
	 * @param listener a listener
	 * @return a registration
	 */
	public Registration addAnimationEndListener(ComponentEventListener<AnimationEndEvent> listener) {
		this.animationEndListenerRegistration = addListener(AnimationEndEvent.class, listener);
		return this.animationEndListenerRegistration;
	}
	
	/**
	 * Method is called after an animation has ended.
	 */
	@Override
	public void handleEvent(DomEvent event) {
		stop();
		Component c = (Component)this.animatedComponent;
		if (effect instanceof HideEffect) {
			c.setVisible(false);
			this.domListenerRegistration.remove();
		} else if (effect instanceof ExitEffect || effect instanceof TextExitEffect) {
			c.getParent().ifPresent(pc -> {
				boolean isContainer = pc instanceof HasComponents;
				if (isContainer) {
					if (domListenerRegistration != null) {
						this.domListenerRegistration.remove();
					}
					HasComponents hc = (HasComponents) pc;
					hc.remove(c);
				}
			});
		} else if (effect instanceof TextEffect) {
			if (domListenerRegistration != null) {
				this.domListenerRegistration.remove();
			}
			HasComponents hc = (HasComponents) this.animatedComponent;
			hc.removeAll();
		}
		ComponentUtil.fireEvent(this, new AnimationEndEvent(c, true));
	}
	
	/**
	 * Detect text animations.
	 * @return true for text animations
	 */
	public boolean isTextEffect() {
		return false;
	}
}