package de.mekaso.vaadin.addon.compani.viewtransitions;

import java.util.Arrays;
import java.util.List;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveEvent.ContinueNavigationAction;
import com.vaadin.flow.router.BeforeLeaveObserver;

import de.mekaso.vaadin.addon.compani.animation.AnimationBuilder;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes;

/**
 * @author Meik Kaufmann
 * An animated view. Override the enterWith, exitWith or animateWith methods to customize animations.
 */
public interface AnimatedView extends HasStyle, BeforeLeaveObserver, BeforeEnterObserver  {

	/**
	 * Set the animation that is shown when entering a view.
	 * @return an view animation
	 */
	public default ViewInTransition enterWith() {
		return ViewInTransition.FadeFromBottom;
	}
	/**
	 * Set the animation that is shown when leaving a view.
	 * @return an animation
	 */
	public default ViewOutTransition exitWith() {
		return ViewOutTransition.FadeToBottom;
	}
	/**
	 * Set the animation for a view (enter and leave)
	 * @return an animation
	 */
	public default ViewTransition animateWith() {
		return ViewTransition.RotateRightAndMoveFromLeft;
	}
	
	public default void beforeLeave(BeforeLeaveEvent event) {
		List<String> classList = null;
		ViewTransition viewTransition = animateWith();
		ViewOutTransition outTransition = exitWith();
		if (viewTransition != null) {
			classList = Arrays.asList(viewTransition.getOutClasses());
		} else if (outTransition != null) {
			classList = Arrays.asList(outTransition.getOutClasses());
		} else {
			return;
		}
		final String[] outClasses = classList.toArray(new String [] {});
		ContinueNavigationAction action = event.postpone();
		AnimationBuilder
		.createBuilderFor(this)
		.create(AnimationTypes.ExitAnimation.class)
		.addAnimationEndListener(listener -> {
			removeClassNames(outClasses);
			action.proceed();
		});
        addClassNames(outClasses);
	}
	
	public default void beforeEnter(BeforeEnterEvent event) {
		List<String> classList = null;
		ViewTransition viewTransition = animateWith();
		ViewInTransition inTransition = enterWith();
		if (viewTransition != null) {
			classList = Arrays.asList(viewTransition.getInClasses());
		} else if (inTransition != null) {
			classList = Arrays.asList(inTransition.getInClasses());
		} else {
			return;
		}
		final String[] inClasses = classList.toArray(new String [] {});
		AnimationBuilder
		.createBuilderFor(this)
		.create(AnimationTypes.ExitAnimation.class)
		.addAnimationEndListener(listener -> {
			removeClassNames(inClasses);
		});
		addClassNames(inClasses);
	}
}
