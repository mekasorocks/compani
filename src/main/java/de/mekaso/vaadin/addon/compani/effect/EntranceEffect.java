package de.mekaso.vaadin.addon.compani.effect;

/**
 * An effect that will work for components that are not currently attached to the UI. The animation will be started when
 * the component is added to the UI. The animation will not be started when the components visibility is switched to true.
 */
public enum EntranceEffect implements Effect {
	backInDown(EffectVendor.AnimateCss),
	backInLeft(EffectVendor.AnimateCss),
	backInRight(EffectVendor.AnimateCss),
	backInUp(EffectVendor.AnimateCss),
	bounceIn(EffectVendor.AnimateCss), 
	bounceInDown(EffectVendor.AnimateCss), 
	bounceInLeft(EffectVendor.AnimateCss), 
	bounceInRight(EffectVendor.AnimateCss), 
	bounceInUp(EffectVendor.AnimateCss),
	fadeIn(EffectVendor.AnimateCss), 
	fadeInDown(EffectVendor.AnimateCss), 
	fadeInDownBig(EffectVendor.AnimateCss), 
	fadeInLeft(EffectVendor.AnimateCss),
	fadeInLeftBig(EffectVendor.AnimateCss), 
	fadeInRight(EffectVendor.AnimateCss), 
	fadeInRightBig(EffectVendor.AnimateCss), 
	fadeInUp(EffectVendor.AnimateCss), 
	fadeInUpBig(EffectVendor.AnimateCss), 
	fadeInTopLeft(EffectVendor.AnimateCss),
	fadeInTopRight(EffectVendor.AnimateCss),
	fadeInBottomLeft(EffectVendor.AnimateCss),
	fadeInBottomRight(EffectVendor.AnimateCss),
	flipInX(EffectVendor.AnimateCss), 
	flipInY(EffectVendor.AnimateCss), 
	jackInTheBox(EffectVendor.AnimateCss), 
	lightSpeedInLeft(EffectVendor.AnimateCss), 
	lightSpeedInRight(EffectVendor.AnimateCss),
	powerOn(EffectVendor.Loading),
	rollIn(EffectVendor.AnimateCss),
	rotateIn(EffectVendor.AnimateCss),
	rotateInDownLeft(EffectVendor.AnimateCss), 
	rotateInDownRight(EffectVendor.AnimateCss), 
	rotateInUpLeft(EffectVendor.AnimateCss), 
	rotateInUpRight(EffectVendor.AnimateCss), 
	slideInUp(EffectVendor.AnimateCss), 
	slideInDown(EffectVendor.AnimateCss), 
	slideInLeft(EffectVendor.AnimateCss),
	slideInRight(EffectVendor.AnimateCss), 
	vortexIn(EffectVendor.Loading),
	zoomIn(EffectVendor.AnimateCss), 
	zoomInDown(EffectVendor.AnimateCss), 
	zoomInLeft(EffectVendor.AnimateCss), 
	zoomInRight(EffectVendor.AnimateCss), 
	zoomInUp(EffectVendor.AnimateCss), 
	;
	private EffectVendor effectVendor;
	private EntranceEffect(EffectVendor effectVendor) {
		this.effectVendor = effectVendor;
	}

	/**
	 * @return the effectVendor
	 */
	public EffectVendor getEffectVendor() {
		return effectVendor;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.COMPONENT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.ENTRANCE;
	}
}