package de.mekaso.vaadin.addon.compani.effect;

/**
 * All animation vendors (CSS libraries used in this component).
 */
public enum EffectVendor {
	/**
	 * Animate.css animations.
	 */
	AnimateCss("animate__animated", "animate__"),
	/**
	 * CssAnimation.io animations.
	 */
	CssAnimation("cssanimation", null),
	/**
	 * CompAni custom animations.
	 */
	CompAni("compani-transitions", "compani-"),
	/**
	 * Dialog animations.
	 */
	Loading("ld", "ld-")
	;
	/**
	 * The CSS Class indicating the vendor.
	 */
	private String cssClass;
	private String prefix;
	/**
	 * Enum Constructor.
	 * @param cssClass the css class
	 * @param prefix the css prefix
	 */
	private EffectVendor(String cssClass, String prefix) {
		this.cssClass = cssClass;
		this.prefix = prefix;
	}
	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}
	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}
}