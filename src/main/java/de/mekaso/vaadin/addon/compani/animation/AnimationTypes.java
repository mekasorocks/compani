package de.mekaso.vaadin.addon.compani.animation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Span;

import de.mekaso.vaadin.addon.compani.effect.AttentionSeeker;
import de.mekaso.vaadin.addon.compani.effect.Delay;
import de.mekaso.vaadin.addon.compani.effect.EffectVendor;
import de.mekaso.vaadin.addon.compani.effect.EntranceEffect;
import de.mekaso.vaadin.addon.compani.effect.ExitEffect;
import de.mekaso.vaadin.addon.compani.effect.HideEffect;
import de.mekaso.vaadin.addon.compani.effect.Repeat;
import de.mekaso.vaadin.addon.compani.effect.ShowEffect;
import de.mekaso.vaadin.addon.compani.effect.Speed;
import de.mekaso.vaadin.addon.compani.effect.TextDisplayEffect;
import de.mekaso.vaadin.addon.compani.effect.TextEffect;
import de.mekaso.vaadin.addon.compani.effect.TextEntranceEffect;
import de.mekaso.vaadin.addon.compani.effect.TextExitEffect;

/**
 * All animation types that can be created with AnimationBuilder.
 */
public class AnimationTypes {
	private static final Logger logger = LoggerFactory.getLogger(AnimationTypes.class);
	/**
	 * @author Meik Kaufmann
	 * Attention seeker animations.
	 */
	public static class AttentionSeekerAnimation extends Animation {
		private static final long serialVersionUID = 1L;
		private Speed speed;
		private Delay delay;
		private Repeat repeat = Repeat.Once;

		/**
		 * Set the animation effect.
		 * @param attentionSeeker the effect
		 * @return an animation
		 */
		public AttentionSeekerAnimation withEffect(AttentionSeeker attentionSeeker) {
			this.effect = attentionSeeker;
			return this;
		}
		
		/**
		 * Set the speed of the animation.
		 * @param speed the speed
		 * @return an animation
		 */
		public AttentionSeekerAnimation withSpeed(Speed speed) {
			this.speed = speed;
			return this;
		}
		
		/**
		 * Set the delay of this animation.
		 * @param delay the delay
		 * @return an animation
		 */
		public AttentionSeekerAnimation withDelay(Delay delay) {
			this.delay = delay;
			return this;
		}
		
		/**
		 * Set the number of repeats for the animation.
		 * @param repeat the repeat
		 * @return an animation
		 */
		public AttentionSeekerAnimation withRepeat(Repeat repeat) {
			this.repeat = repeat;
			return this;
		}
		
		/**
		 * Fetch all CSS classes that need to be added to the component.
		 * @return the CSS classes
		 */
		protected String[] fetchCssClasses() {
			List<String> cssClasses = new ArrayList<>();
			if (ObjectUtils.isNotEmpty(this.effect.getEffectVendor().getCssClass())) {
				cssClasses.add(this.effect.getEffectVendor().getCssClass());
			}
			String sEffect = this.effect.getEffectVendor().getPrefix() != null ? this.effect.getEffectVendor().getPrefix() + this.effect.name() : this.effect.name();
			cssClasses.add(sEffect);
			if (this.delay != null && this.delay != Delay.noDelay) {
				cssClasses.add(this.delay.getCssClassName());
			}
			if (this.speed != null && this.speed != Speed.normal) {
				cssClasses.add(this.effect.getEffectVendor().getPrefix() + this.speed.name());
			}
			if (this.repeat != null && this.repeat != Repeat.Once) {
				cssClasses.add(this.repeat.getCssClassName());
			}
			return cssClasses.toArray(new String [] {});
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * An animation that will be show when a component is added to the UI.
	 */
	public static class EntranceAnimation extends Animation {
		private static final long serialVersionUID = 1L;
		private Speed speed;
		private Delay delay;
		
		/**
		 * Set the animation effect
		 * @param effect the effect
		 * @return an animation
		 */
		public EntranceAnimation withEffect(EntranceEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * @param speed the speed
		 * @return an animation
		 */
		public EntranceAnimation withSpeed(Speed speed) {
			this.speed = speed;
			return this;
		}
		
		/**
		 * @param delay the delay
		 * @return an animation
		 */
		public EntranceAnimation withDelay(Delay delay) {
			this.delay = delay;
			return this;
		}
		
		/**
		 * Register the effect for the animated component.
		 */
		public void register() {
			((Component)this.animatedComponent).addAttachListener(attachEvent -> {
				start();
			});
		}

		@Override
		protected String[] fetchCssClasses() {
			List<String> cssClasses = new ArrayList<>();
			String prefix = this.effect.getEffectVendor().getPrefix() != null ? this.effect.getEffectVendor().getPrefix() : "";
			cssClasses.add(this.effect.getEffectVendor().getCssClass());
			if (this.effect.getEffectVendor() == EffectVendor.Loading) {
				cssClasses.add(prefix + camelCaseToCss(this.effect.name()));
			} else {
				cssClasses.add(prefix + this.effect.name());
			}
			if (this.delay != null && this.delay != Delay.noDelay) {
				cssClasses.add(this.delay.getCssClassName());
			}
			if (this.speed != null && this.speed != Speed.normal) {
				cssClasses.add(prefix + this.speed.name());
			}
			return cssClasses.toArray(new String [] {});
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Animate a component that is made visible.
	 *
	 */
	public static class ShowAnimation extends Animation {
		private static final long serialVersionUID = 1L;
		
		/**
		 * Set the effect of the animation.
		 * @param effect the effect
		 * @return an animation
		 */
		public ShowAnimation withEffect(ShowEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * Animate and show the component.
		 */
		public void show() {
			((Component)this.animatedComponent).setVisible(true);
			start();
		}
		
		@Override
		protected String[] fetchCssClasses() {
			List<String> cssClasses = new ArrayList<>();
			String prefix = this.effect.getEffectVendor().getPrefix() != null ? this.effect.getEffectVendor().getPrefix() : "";
			cssClasses.add(this.effect.getEffectVendor().getCssClass());
			if (this.effect.getEffectVendor() == EffectVendor.Loading) {
				cssClasses.add(prefix + camelCaseToCss(this.effect.name()));
			} else {
				cssClasses.add(prefix + this.effect.name());
			}
			return cssClasses.toArray(new String [] {});
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Show an animation before hiding a component.
	 */
	public static class HideAnimation extends Animation {
		private static final long serialVersionUID = 1L;
		/**
		 * Set the effect for an animation.
		 * @param effect the effect 
		 * @return an animation
		 */
		public HideAnimation withEffect(HideEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * Animate and hide a component.
		 */
		public void hide() {
			start();
		}
		
		@Override
		protected String [] fetchCssClasses() {
			List<String> cssClasses = new ArrayList<>();
			String prefix = this.effect.getEffectVendor().getPrefix() != null ? this.effect.getEffectVendor().getPrefix() : "";
			cssClasses.add(this.effect.getEffectVendor().getCssClass());
			if (this.effect.getEffectVendor() == EffectVendor.Loading) {
				cssClasses.add(prefix + camelCaseToCss(this.effect.name()));
			} else {
				cssClasses.add(prefix + this.effect.name());
			}
			return cssClasses.toArray(new String [] {});
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Animate a component before removing it from the UI.
	 */
	public static class ExitAnimation extends EntranceAnimation {
		private static final long serialVersionUID = 1L;

		/**
		 * Set the effect for this animation.
		 * @param effect the effect
		 * @return an animation
		 */
		public ExitAnimation withEffect(ExitEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * Animate and remove a component.
		 */
		public void remove() {
			start();
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Animation for text component (that implement HasText).
	 */
	public static class TextAnimation extends Animation {
		private static final long serialVersionUID = 1L;
		
		private String originalContent;

		/**
		 * Set the effect for a text component.
		 * @param effect the effect
		 * @return an animation
		 */
		public TextAnimation withEffect(TextDisplayEffect effect) {
			this.effect = effect;
			return this;
		}

		@Override
		protected String[] fetchCssClasses() {
			return new String [] {this.effect.getEffectVendor().getCssClass(), ((TextEffect)this.effect).getCssClass(), "sequence"};
		}
		
		@Override
		public void start() {
			super.start();
			startTextAnimation();
		}
		
		/**
		 * Start the text animation.
		 */
		protected void startTextAnimation() {
			Component c = (Component)this.animatedComponent;
			if (c instanceof HasComponents) {
				HasComponents container = (HasComponents) c;
				HasText hasText = (HasText) this.animatedComponent;
				originalContent = hasText.getText();
				if (originalContent.isEmpty()) {
					return;
				}
				hasText.setText("");
				logger.debug("Remember original content '{}'", originalContent);
				char[] content = originalContent.toCharArray();
				long delay = 150;
				for (int index = 0; index < content.length; index++) {
					char letter = content[index];
					Span span = new Span(String.valueOf(letter));
					if (letter != ' ') {
						span.getStyle().set("-webkit-animation-delay", String.valueOf(delay) + "ms");
						span.getStyle().set("animation-delay", String.valueOf(delay) + "ms");
						span.getStyle().set("-moz-animation-delay", String.valueOf(delay) + "ms");
						delay += 150;
					} else {
						span.setWidth(0.3f, Unit.EM);
					}
					container.add(span);
					if (index == content.length - 1) {
						domListenerRegistration = span.getElement().addEventListener(AnimationEndEvent.NAME, this);
						span.addDetachListener(detachEvent -> {
							logger.debug("Setting content back to '{}'", originalContent);
							hasText.setText(originalContent);
							stop();
						});
					}
				}
				this.currentEffect = this.fetchCssClasses();
				this.animatedComponent.addClassNames(this.currentEffect);
			} 
		}
		
		@Override
		public boolean isTextEffect() {
			return true;
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Animate a text component when adding it to the UI.
	 */
	public static class TextEntranceAnimation extends TextAnimation {
		private static final long serialVersionUID = 1L;
		/**
		 * Set the effect for an animation.
		 * @param effect the effect
		 * @return an animation
		 */
		public TextEntranceAnimation withEffect(TextEntranceEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * Register the animation for a text component.
		 */
		public void register() {
			Component c = (Component)this.animatedComponent;
			c.addAttachListener(attachEvent -> {
				startTextAnimation();
			});
		}
		
		@Override
		public boolean isTextEffect() {
			return true;
		}
	}
	
	/**
	 * @author Meik Kaufmann
	 * Animate a text component before removing it from the UI.
	 */
	public static class TextExitAnimation extends TextAnimation {
		private static final long serialVersionUID = 1L;

		/**
		 * Set the effect for an animation.
		 * @param effect the effect
		 * @return an animation
		 */
		public TextExitAnimation withEffect(TextExitEffect effect) {
			this.effect = effect;
			return this;
		}
		
		/**
		 * Animate and remove the component.
		 */
		public void remove() {
			start();
		}
		
		@Override
		public boolean isTextEffect() {
			return true;
		}
	}
	
	private static String camelCaseToCss(String input) {
		String regex = "([a-z])([A-Z]+)";
        String replacement = "$1-$2";
        return input.replaceAll(regex, replacement)
                .toLowerCase();
	}
}
