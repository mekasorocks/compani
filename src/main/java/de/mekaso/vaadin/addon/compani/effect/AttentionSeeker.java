package de.mekaso.vaadin.addon.compani.effect;

/**
 * Animation effects for components that are attached to the UI.
 */
public enum AttentionSeeker implements Effect {
	bounce(EffectVendor.AnimateCss), 
	flash(EffectVendor.AnimateCss), 
	flip(EffectVendor.AnimateCss),
	heartBeat(EffectVendor.AnimateCss), 
	jello(EffectVendor.AnimateCss), 
	pulse(EffectVendor.AnimateCss), 
	rubberBand(EffectVendor.AnimateCss), 
	shakeX(EffectVendor.AnimateCss), 
	shakeY(EffectVendor.AnimateCss),
	headShake(EffectVendor.AnimateCss), 
	swing(EffectVendor.AnimateCss), 
	tada(EffectVendor.AnimateCss), 
	wobble(EffectVendor.AnimateCss), 

	growOnHover(EffectVendor.CompAni), 
	shrinkOnHover(EffectVendor.CompAni), 
	;

	private EffectVendor effectVendor;
	private AttentionSeeker(EffectVendor effectVendor) {
		this.effectVendor = effectVendor;
	}
	
	/**
	 * @return the vendor
	 */
	public EffectVendor getEffectVendor() {
		return effectVendor;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.COMPONENT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.DISPLAY;
	}
}