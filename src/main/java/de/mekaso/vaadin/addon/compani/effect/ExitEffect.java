package de.mekaso.vaadin.addon.compani.effect;

/**
 * The effect will work for all components that are attached to the UI. The animation will be started when the component 
 * is removed from the UI. The animation will not be started, if the visibility of the component is switched to false.
 */
public enum ExitEffect implements Effect {
	backOutDown(EffectVendor.AnimateCss), 
	backOutLeft(EffectVendor.AnimateCss), 
	backOutRight(EffectVendor.AnimateCss), 
	backOutUp(EffectVendor.AnimateCss), 
	bounceOut(EffectVendor.AnimateCss), 
	bounceOutDown(EffectVendor.AnimateCss), 
	bounceOutLeft(EffectVendor.AnimateCss), 
	bounceOutRight(EffectVendor.AnimateCss), 
	bounceOutUp(EffectVendor.AnimateCss), 
	fadeOut(EffectVendor.AnimateCss), 
	fadeOutDown(EffectVendor.AnimateCss), 
	fadeOutDownBig(EffectVendor.AnimateCss),
	fadeOutLeft(EffectVendor.AnimateCss), 
	fadeOutLeftBig(EffectVendor.AnimateCss), 
	fadeOutRight(EffectVendor.AnimateCss), 
	fadeOutRightBig(EffectVendor.AnimateCss), 
	fadeOutUp(EffectVendor.AnimateCss), 
	fadeOutUpBig(EffectVendor.AnimateCss), 
	fadeOutTopLeft(EffectVendor.AnimateCss), 
	fadeOutTopRight(EffectVendor.AnimateCss), 
	fadeOutBottomRight(EffectVendor.AnimateCss), 
	fadeOutBottomLeft(EffectVendor.AnimateCss), 
	flipOutX(EffectVendor.AnimateCss), 
	flipOutY(EffectVendor.AnimateCss),
	hinge(EffectVendor.AnimateCss),
	lightSpeedOutLeft(EffectVendor.AnimateCss), 
	lightSpeedOutRight(EffectVendor.AnimateCss),
	powerOff(EffectVendor.Loading),
	rollOut(EffectVendor.AnimateCss),
	rotateOut(EffectVendor.AnimateCss), 
	rotateOutDownLeft(EffectVendor.AnimateCss), 	
	rotateOutDownRight(EffectVendor.AnimateCss), 
	rotateOutUpLeft(EffectVendor.AnimateCss), 
	rotateOutUpRight(EffectVendor.AnimateCss), 
	slideOutDown(EffectVendor.AnimateCss), 
	slideOutLeft(EffectVendor.AnimateCss), 
	slideOutRight(EffectVendor.AnimateCss), 
	slideOutUp(EffectVendor.AnimateCss),
	vortexOut(EffectVendor.Loading),
	zoomOut(EffectVendor.AnimateCss), 
	zoomOutDown(EffectVendor.AnimateCss), 
	zoomOutLeft(EffectVendor.AnimateCss), 
	zoomOutRight(EffectVendor.AnimateCss), 
	zoomOutUp(EffectVendor.AnimateCss), 
	;
	
	private EffectVendor effectVendor;
	private ExitEffect(EffectVendor effectVendor) {
		this.effectVendor = effectVendor;
	}

	@Override
	public AnimationType getAnimationType() {
		return AnimationType.COMPONENT;
	}

	@Override
	public AnimationStage getAnimationStage() {
		return AnimationStage.EXIT;
	}

	@Override
	public EffectVendor getEffectVendor() {
		return this.effectVendor;
	}
}