package de.mekaso.vaadin.addon.compani.effect;

/**
 * Interface for all text effects.
 */
public interface TextEffect extends Effect {
	/**
	 * The CSS class to apply.
	 * @return the css class of the effect
	 */
	public String getCssClass();
}
