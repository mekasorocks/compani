package de.mekaso.vaadin.addon.compani.effect;

/**
 * A delay before an animation starts.
 */
public enum Delay {
	noDelay(""),
	two("delay-2s"),
	three("delay-3s"),
	four("delay-4s"),
	five("delay-5s")
	;
	private String cssClassName;
	private Delay(String cssClassName) {
		this.cssClassName = cssClassName;
	}
	/**
	 * @return the cssClassName
	 */
	public String getCssClassName() {
		if (this != noDelay) {
			return String.format("animate__%s", cssClassName);
		}
		return "";
	}
}
