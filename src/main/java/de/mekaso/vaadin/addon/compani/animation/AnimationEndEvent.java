package de.mekaso.vaadin.addon.compani.animation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DebounceSettings;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.dom.DebouncePhase;

/**
 * @author Meik Kaufmann
 * Animation end event.
 */
@DomEvent(
		value = AnimationEndEvent.NAME,
		debounce = @DebounceSettings(timeout = 250, phases = DebouncePhase.TRAILING))
public class AnimationEndEvent extends ComponentEvent<Component> {
	private static final long serialVersionUID = 1L;
	/**
	 * Name of the DOM event for an ending CSS transition.
	 */
	public static final String NAME = "webkitAnimationEnd";

	/**
	 * Constructor.
	 * @param source source component
	 * @param fromClient from client
	 */
	public AnimationEndEvent(Component source, boolean fromClient) {
		super(source, fromClient);
	}
}