package de.mekaso.vaadin.addon.compani.effect;

public enum Repeat {
	Once("repeat-1"), 
	Twice("repeat-2"), 
	ThreeTimes("repeat-3"), 
	Infinite("infinite")
	;
	private String cssClassName;
	private Repeat(String cssClassName) {
		this.cssClassName = cssClassName;
	}
	/**
	 * @return the cssClassName
	 */
	public String getCssClassName() {
		if (this != Once) {
			return String.format("animate__%s", cssClassName);
		}
		return "";
	}
}
